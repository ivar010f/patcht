"""Pytest fixture version of patch module.

Notes:
    Default decorator version of :fun:`patcht.patch` does not
    work together with ``@pytest.mark.parametrize``. Patched
    mocks will not reset between parametrized test executions.
"""
from typing import (
    overload,
    Callable,
    Any,
    TypeVar,
    Union,
    Iterable,
    Type,
    List,
    Generator,
)
from unittest import mock

import pytest
from attr import dataclass, attrib

from .patch import patch as _patch

T = TypeVar("T")

SideEffect = Union[T, Iterable[T], Exception, Type[Exception]]
TCallable = Callable[..., T]


@dataclass
class Patcher:
    patched_objects: List[Any] = attrib(factory=list)

    @overload
    def patch(self, target: T, new: Any = ...) -> T:
        ...

    @overload
    def patch(self, target: TCallable, new: Any = ..., *, return_value: T) -> TCallable:
        ...

    @overload
    def patch(
        self, target: TCallable, new: Any = ..., *, side_effect: SideEffect
    ) -> TCallable:
        ...

    def patch(
        self,
        target,
        new=mock.DEFAULT,
        *,
        return_value=mock.DEFAULT,
        side_effect=mock.DEFAULT
    ):
        patched_object = _patch(
            target, new=new, return_value=return_value, side_effect=side_effect
        )  # noqa
        self.patched_objects.append(patched_object)
        return patched_object.start()

    def stop_all(self):
        for patched_object in self.patched_objects:
            patched_object.stop()


@pytest.fixture
def patcher() -> Generator[Patcher, None, None]:
    patcher_ = Patcher()
    yield patcher_
    patcher_.stop_all()
