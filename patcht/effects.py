"""Side effects and assert functions for symbolic patch target mocks."""
from typing import (
    TypeVar,
    cast,
    Sequence,
    Any,
    Callable,
    overload,
    Union,
    Iterable,
    Type,
)
from unittest import mock

from .target import SymbolicPatchTarget, TargetedMockCall

T = TypeVar("T")
SideEffect = Union[T, Iterable[T], Exception, Type[Exception]]
TCallable = Callable[..., T]


@overload
def return_value(target: TCallable) -> T:
    ...


@overload
def return_value(target: TCallable, new: T) -> None:
    ...


def return_value(target, new=mock.DEFAULT):
    """Set or get return value for mock."""
    resolved = resolve_mock(target)
    if new is mock.DEFAULT:
        return resolved.return_value

    resolved.return_value = new


def resolve_mock(target) -> mock.Mock:
    """Resolve the mock object of a symbolic patch target."""
    assert isinstance(target, SymbolicPatchTarget)
    resolved = cast(SymbolicPatchTarget, target).resolve()
    assert hasattr(resolved, "_mock_children")
    return resolved


@overload
def side_effect(target: TCallable) -> SideEffect:
    ...


@overload
def side_effect(target: TCallable, new: SideEffect = None) -> None:
    ...


def side_effect(target, new=None) -> None:
    """Set or get side effect for mock."""
    resolved = resolve_mock(target)
    if new is mock.DEFAULT:
        return resolved.side_effect

    resolved.side_effect = new


def call_count(target: Any) -> int:
    """Get call count of a symbolic patch target callable."""
    return resolve_mock(target).call_count


def assert_has_calls(*target_calls: Any) -> None:
    """Assert a group of mock calls has been made."""
    mock_calls = cast(Sequence[TargetedMockCall], target_calls)
    if not mock_calls:
        return

    mock_obj = mock_calls[0].mock
    assert all(mock_call.mock is mock_obj for mock_call in mock_calls)
    mock_obj.assert_has_calls([mock_call.mock_call for mock_call in mock_calls])
