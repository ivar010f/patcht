"""Provides functions for type hinted patching and mocking."""
from .effects import side_effect, return_value, assert_has_calls, call_count
from .patch import patch, patch_inline
from .patcher import patcher
from .target import target
