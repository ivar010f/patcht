"""Implements symbolic patch target support for python unittest mocking."""
from typing import Type, TypeVar, cast, Sequence, no_type_check
from unittest import mock

from attr import dataclass

T = TypeVar("T")


@no_type_check
def target(module: T) -> T:
    """Creates a symbolic patch target, that allows for type checking."""
    return cast(Type[T], SymbolicPatchTarget(module))


@dataclass
class SymbolicPatchTarget:
    module: T
    items: Sequence[str] = ()

    def __getattr__(self, item):
        return SymbolicPatchTarget(self.module, (*self.items, item))

    def __str__(self):
        return ".".join((self.module.__name__, *self.items))

    def __call__(self, *args, **kwargs):
        return TargetedMockCall(self, args, kwargs)

    def resolve(self):
        def resolve_item(base, items):
            if not items:
                return base
            item, *tail = items
            return resolve_item(getattr(base, item), tail)

        return resolve_item(self.module, self.items)


@dataclass(cmp=False)
class TargetedMockCall:
    target: SymbolicPatchTarget
    args: tuple
    kwargs: dict

    @property
    def mock_call(self):
        return mock.call(*self.args, **self.kwargs)

    @property
    def mock(self):
        return self.target.resolve()

    def __eq__(self, other):
        return self.mock_call == other
