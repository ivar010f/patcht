"""Patch functions for symbolic patch target."""
from typing import Type, TypeVar, cast, Any, Callable, Union, Iterable, overload
from unittest import mock

from .target import SymbolicPatchTarget

T = TypeVar("T")

SideEffect = Union[T, Iterable[T], Exception, Type[Exception]]
TCallable = Callable[..., T]


@overload
def patch(target: Any, new: Any = ...):
    ...


@overload
def patch(target: TCallable, new: Any = ..., *, return_value: T):
    ...


@overload
def patch(target: TCallable, new: Any = ..., *, side_effect: SideEffect):
    ...


def patch(
    target: Any,
    new=mock.DEFAULT,
    *,
    return_value=mock.DEFAULT,
    side_effect=mock.DEFAULT
):
    """Symbolic target version of ``unittest.mock.patch``, using autospec=True."""
    if return_value is not mock.DEFAULT or side_effect is not mock.DEFAULT:
        new = new is not mock.DEFAULT and new or _create_mock(target)
        if return_value is not mock.DEFAULT:
            new.return_value = return_value
        if side_effect is not mock.DEFAULT:
            new.side_effect = side_effect

    autospec = new is mock.DEFAULT
    return mock.patch(str(target), new=new, autospec=autospec)


def _create_mock(target) -> mock.MagicMock:
    spec = cast(SymbolicPatchTarget, target).resolve()
    return mock.MagicMock(spec=spec)


@overload
def patch_inline(target: Any, new: Any = ..., *, return_value: None = None):
    # return_value keyword added due to PyCharm error
    ...


@overload
def patch_inline(target: TCallable, new: Any = ..., *, return_value: T):
    ...


@overload
def patch_inline(target: TCallable, new: Any = ..., *, side_effect: SideEffect):
    ...


def patch_inline(target: Any, *, return_value=mock.DEFAULT, side_effect=None):
    """Like patch, but does not give mocked object as function input.

    This method prompts the user to use a ``SymbolicPatchTarget``
    reference to the mocked object. This results in tool assist for the
    mocked object, since the type information is preserved.
    """
    new = _create_mock(target)
    return patch(
        target, new=new, return_value=return_value, side_effect=side_effect
    )  # noqa
